package logic

import (
	"encoding/json"
	"crypto/aes"
	"io"
	"crypto/rand"
	"crypto/cipher"
	"fmt"
	"encoding/hex"
	"errors"
	"time"
)


var (
	CYPHER_KEY = "hghjgYG#t7@3t6iyg%7h^$gghhtg&&*k"
)
//Generate a token
func GenerateToken(userID string) (string, error) {
	expiry := time.Now().Add(72 * time.Hour).Unix()
	payload := map[string]interface{}{
		"userId": userID,
		"expiry": expiry,
	}
	pJson, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}

	key := []byte(CYPHER_KEY)
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, aes.BlockSize + len(pJson))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], pJson)

	return fmt.Sprintf("%x", ciphertext), nil
}

//Decrypt a token
func DecryptToken(token string) (string, error) {
	key := []byte(CYPHER_KEY)
	ciphertext, _ := hex.DecodeString(token)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("Cyphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)
	payload := fmt.Sprintf("%s", ciphertext)

	pObj := make(map[string]interface{})

	err = json.Unmarshal([]byte(payload), &pObj)
	if err != nil {
		return "", err
	}

	expiry, _ := pObj["expiry"]
	if int64(expiry.(float64)) < time.Now().Unix() {
		return "", fmt.Errorf("Time of life of token is expiried")
	}

	userID, _ := pObj["userId"]

	return  userID.(string), nil
}