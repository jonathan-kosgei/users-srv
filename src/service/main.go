package main

import (
	"log"
	"time"

	"github.com/micro/go-micro"
	"gitlab.contetto.io/contetto-micro/users-srv/src/service/db"
	"gitlab.contetto.io/contetto-micro/users-srv/src/service/handler"
	"gitlab.contetto.io/contetto-micro/users-srv/src/service/restful"
	"gitlab.contetto.io/contetto-micro/producer"
	proto "gitlab.contetto.io/contetto-micro/users-srv/src/service/proto/account"
	 _ "github.com/micro/go-plugins/broker/rabbitmq"
	 //_ "gitlab.contetto.io/contetto-micro/kuber-plugin"
)


func main() {
	service := micro.NewService(
		micro.Name("users"),
		micro.RegisterTTL(time.Second*30),
		micro.RegisterInterval(time.Second*10),
	)

	service.Init()
	producer.InitProducer(service.Options().Broker)
	db.Init()
	go restful.Init()

	proto.RegisterAccountHandler(service.Server(), new(handler.Account))
	restful.Micro = service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

