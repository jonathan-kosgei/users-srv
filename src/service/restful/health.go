package restful

import (
	"log"
	"net/http"

	"gitlab.contetto.io/contetto-micro/users-srv/src/service/db"
)

type HealthHandler struct {
  
}

// Health check, that every compinent from the service is works
func Health(w http.ResponseWriter, r *http.Request) {
	// Check db connection
	log.Printf("Checking db connection for the the %s service", ServiceName)
	err := db.Ping()
	if err != nil {
		log.Printf("unable to connect for the mongodb on %s service: %v", ServiceName, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("connection for the mongodb on %s service is works", ServiceName)
	w.WriteHeader(http.StatusOK)
}
