package handler

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/micro/go-micro/client"
	"gitlab.contetto.io/contetto-micro/producer"
	"gitlab.contetto.io/contetto-micro/users-srv/src/service/db"
	logic "gitlab.contetto.io/contetto-micro/users-srv/src/service/logic"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"

	roles "gitlab.contetto.io/contetto-micro/roles-srv/src/service/proto/roles"
	emails "gitlab.contetto.io/contetto-micro/sending-email-srv/src/service/proto/email"
	account "gitlab.contetto.io/contetto-micro/users-srv/src/service/proto/account"
)

const (
	x = "cruft123"
)

var (
	alphanum  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	cypherKey = "hghjgYG#t7@3t6iyg%7h^$gghhtg&&*k"
)

var (
	ErrComparePasswords = errors.New("Invalid old password")
	TopicEvents         = "events"
)

type Account struct{}

func (s *Account) Create(ctx context.Context, req *account.User, rsp *account.CreateResponse) error {
	//salt := random(16)
	//h, err := bcrypt.GenerateFromPassword([]byte(x+salt+req.Password), 10)
	log.Println("On the Create method")
	//nativePass := req.Password
	h, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	pp := base64.StdEncoding.EncodeToString(h)
	id := bson.NewObjectId()
	req.Id = id.Hex()
	req.Password = pp
	req.Username = strings.ToLower(req.Username)
	req.Email = strings.ToLower(req.Email)

	err = validation(req)
	if err != nil {
		return err
	}

	log.Println("After validation of request")

	//Generate API ID and Secret
	unix32bits := uint32(time.Now().UTC().Unix())
	buff := make([]byte, 12)
	numRead, err := rand.Read(buff)
	if numRead != len(buff) || err != nil {
		rand.Read(buff)
	}
	buff2 := make([]byte, 8)
	rand.Read(buff2)
	appID := fmt.Sprintf("%x%x@%s", unix32bits, buff2[0:], req.Platform)
	appSecret := fmt.Sprintf("%x-%x-%x-%x-%x-%x", unix32bits, buff[0:2], buff[2:4], buff[4:6], buff[6:8], buff[8:])
	req.AppId = appID
	req.AppSecret = appSecret
	req.IsVerified = false

	// Generation of verification token
	token, err := logic.GenerateToken(id.Hex())
	if err != nil {
		return err
	}

	rsp.Id = id.Hex()
	rsp.ValidationToken = token
	err = db.Create(req, "", "")
	// Return new id
	//err = db.Create(req.User, salt, pp)
	if err != nil {
		return err
	}

	if !req.NotSendEmail {
		fmt.Println("Send email")
		//go sendEmail(req.Email, nativePass, token)
	}

	producer.Publish(TopicEvents, map[string]string{"id": id.Hex()}, map[string]string{"test": "test"})
	return nil
}

func (s *Account) Read(ctx context.Context, req *account.ReadRequest, rsp *account.ReadResponse) error {
	user, err := db.Read(req.Id)
	if err != nil {
		return err
	}
	rsp.User = user
	return nil
}

func (s *Account) Update(ctx context.Context, req *account.UpdateRequest, rsp *account.UpdateResponse) error {
	req.User.Username = strings.ToLower(req.User.Username)
	req.User.Email = strings.ToLower(req.User.Email)

	// If we update password
	if req.User.OldPassword != "" && req.User.NewPassword != "" {
		err := ResetPassword(&account.UpdatePasswordRequest{OldPassword: req.User.OldPassword, NewPassword: req.User.NewPassword, SessionId: req.SessionId, UserId: req.Id})
		if err != nil {
			return err
		}
		req.User.OldPassword = ""
		req.User.NewPassword = ""
		return nil
	}
	return db.Update(req.Id, bson.M{"$set": req.User})
}

func (s *Account) Delete(ctx context.Context, req *account.DeleteRequest, rsp *account.DeleteResponse) error {
	return db.Delete(req.Id)
}

func (s *Account) Search(ctx context.Context, req *account.SearchRequest, rsp *account.SearchResponse) error {
	query := bson.M{}
	if req.Email != "" {
		query["email"] = req.Email
	}
	if req.Username != "" {
		query["username"] = req.Username
	}
	if req.Country != "" {
		query["country"] = req.Country
	}
	if req.City != "" {
		query["city"] = req.City
	}
	users, err := db.Search(query)
	if err != nil {
		return err
	}
	rsp.Users = users
	return nil
}

func (s *Account) GetUserByEmail(ctx context.Context, req *account.SearchRequest, rsp *account.SearchResponseOne) error {
	user, err := db.FindOne(bson.M{"email": req.Email})
	if err != nil {
		return err
	}
	rsp.User = &user
	return nil
}

func (s *Account) ResetPassword(ctx context.Context, req *account.ResetPasswordRequest, res *account.ResetPasswordResponse) error {
	usr, err := db.Read(req.UserID)
	if err != nil {
		return err
	}
	pass, err := base64.StdEncoding.DecodeString(usr.Password)
	if err != nil {
		return err
	}
	fmt.Println(usr.Password, req.OldPassword)
	// Compare old password
	if err := bcrypt.CompareHashAndPassword(pass, []byte(req.OldPassword)); err != nil {
		return err
	}

	h, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), 10)
	if err != nil {
		return err
	}
	pp := base64.StdEncoding.EncodeToString(h)

	// Store new password to user
	err = db.UpdatePassword(req.UserID, pp)
	if err != nil {
		log.Printf("Not found")
		return err
	}

	res.Session = &account.Session{UserId: usr.Username}

	// Delete old session
	return db.DeleteSession(req.SessionID)
}

func (s *Account) LoginNative(ctx context.Context, req *account.LoginRequest, rsp *account.LoginResponse) error {
	//username := strings.ToLower(req.Username)
	email := strings.ToLower(req.Email)

	/*salt, hashed, err := db.SaltAndPassword(username, email)
	if err != nil {
		return err
	}

	hh, err := base64.StdEncoding.DecodeString(hashed)
	if err != nil {
		return errors.InternalServerError("go.micro.srv.user.Login", err.Error())
	}*/

	hh, username, err := db.GetPassword(email, "")
	if err != nil {
		return err
	}

	pass, err := base64.StdEncoding.DecodeString(hh)
	if err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword(pass, []byte(req.Password)); err != nil {
		return err
	}

	existUser, err := db.FindOne(bson.M{"email": email})
	if err != nil {
		return err
	}
	Id := random(128)

	// Save session
	sess := &account.Session{
		SessionId: Id,
		UserId:    existUser.Id,
		Username:  username,
		Created:   time.Now().Unix(),
		Expires:   time.Now().Add(time.Hour * 24 * 7).Unix(),
	}
	// Store sessions
	if err := db.CreateSession(sess); err != nil {
		return err
	}

	rsp.Session = sess
	//producer.Publish(map[string]string{"Action": "login", "Username": username})
	return nil
}

func (s *Account) SocialLogin(ctx context.Context, req *account.SocialLoginRequest, rsp *account.SocialLoginResponse) error {
	existUser, err := db.FindOne(bson.M{"email": req.Email})
	if err != nil {
		return err
	}
	Id := random(128)

	// Save session
	sess := &account.Session{
		SessionId: Id,
		UserId:    existUser.Id,
		Username:  existUser.Username,
		Created:   time.Now().Unix(),
		Expires:   time.Now().Add(time.Hour * 24 * 7).Unix(),
	}
	rsp.Session = sess
	// Store sessions
	if err := db.CreateSession(sess); err != nil {
		return err
	}

	return nil
}

func (s *Account) Logout(ctx context.Context, req *account.LogoutRequest, rsp *account.LogoutResponse) error {
	return db.DeleteSession(req.SessionId)
}

func (s *Account) ValidateUser(ctx context.Context, req *account.ReadSessionRequest, rsp *account.ReadSessionResponse) error {
	sess, err := db.ReadSession(req.SessionId)
	if err != nil {
		return err
	}
	rsp.Session = &sess
	return nil
}

func (s *Account) GetUserBySession(ctx context.Context, req *account.ReadSessionRequest, rsp *account.UserResponse) error {
	usr, err := db.GetUserBySession(req.SessionId)
	if err != nil {
		return err
	}

	rsp.User = usr
	return nil
}

func (s *Account) UpdateSession(ctx context.Context, req *account.UpdateSessionRequest, rsp *account.UpdateSessionResponse) error {
	fmt.Println(req.SessionId)
	oldSession, err := db.ReadSession(req.SessionId)
	if err != nil {
		fmt.Println("Unable to read a session")
		return err
	}

	// read user object
	userItem, err := db.Read(oldSession.UserId)
	if err != nil {
		fmt.Println("Unable to read a user")
		return err
	}

	companyId := userItem.CompanyId
	brandId := userItem.BrandId
	query := bson.M{}
	if req.CompanyId != "" {
		query["companyid"] = req.CompanyId
		companyId = req.CompanyId
	}

	if req.BrandId != "" {
		query["brandid"] = req.BrandId
		brandId = req.BrandId
	}
	Id := random(128)

	// Save session
	sess := &account.Session{
		SessionId: Id,
		UserId:    oldSession.UserId,
		CompanyId: companyId,
		BrandId:   brandId,
		Created:   time.Now().Unix(),
		Expires:   time.Now().Add(time.Hour * 24 * 7).Unix(),
	}

	// Check, if role for the user is exist
	cl := roles.NewRolesClient("roles", client.DefaultClient)
	_, err = cl.SearchUserRoles(context.TODO(), &roles.SearchUserRolesRequest{UserId: oldSession.UserId, CompanyId: companyId})
	if err != nil {
		return err
	}

	// Store sessions
	if err := db.CreateSession(sess); err != nil {
		fmt.Println("Unable to create a new session")
		return err
	}

	err = db.Update(oldSession.UserId, bson.M{"$set": query})
	if err != nil {
		fmt.Println("Unable to update")
		return err
	}

	// Delete old session
	err = db.DeleteSession(oldSession.SessionId)
	if err != nil {
		fmt.Println("Unable to delete a session")
		return errors.New("Old session was not deleted")
	}

	rsp.Session = sess

	return nil
}

func (s *Account) VerifyUser(ctx context.Context, req *account.VerifyRequest, rsp *account.VerifyResponse) error {
	ID, err := logic.DecryptToken(req.Token)
	if err != nil {
		return err
	}

	// Check if user is already verified
	usr, err := db.Read(ID)
	if err != nil {
		return err
	}

	if usr.IsVerified {
		return fmt.Errorf("User with id %s is already verivied", ID)
	}
	// Make user as verivied
	return db.Update(ID, bson.M{"$set": bson.M{"isverified": true}})
}

func (s *Account) StoreEndpoint(ctx context.Context, req *account.StoreEndpointRequest, rsp *account.StoreEndpointResponse) error {
	id := bson.NewObjectId()
	req.Id = id.Hex()
	err := db.StoreLog(req)
	if err != nil {
		return err
	}

	rsp.Id = id.Hex()
	return nil
}

func (s *Account) GetEndpoints(ctx context.Context, req *account.GetEndpointsRequest, rsp *account.GetEndpointsResponse) error {
	logsItems, err := db.GetLogs(bson.M{"userid": req.UserId})
	if err != nil {
		return err
	}

	rsp.Logs = logsItems
	return nil
}

func (s *Account) Health(ctx context.Context, req *account.HealthItem, rsp *account.HealthItem) error {
	rsp.Result = "OK"
	return nil
}

// HashPassword hash the passed password using sha1
func HashPassword(pass string) string {
	h := sha1.New()
	h.Write([]byte(pass))
	sha1Hash := hex.EncodeToString(h.Sum(nil))
	return sha1Hash
}

func validation(req *account.User) error {
	if !govalidator.IsEmail(req.Email) {
		return fmt.Errorf("%s is not email address")
	}

	return nil
}

func sendEmail(email, password, token string) {
	link := fmt.Sprintf("https://app.contetto.io/auth/verification?token=%s", token)
	emailSend := &emails.ConfirmEmailRequest{Email: email, Password: password, Link: link}
	// Sending email
	reqEmails := client.NewRequest("sending-emails", "SendingEmail.EmailConfirmation", emailSend)

	rspEmails := &emails.EmailEmptyResponse{}
	err := client.Call(context.Background(), reqEmails, rspEmails)
	if err != nil {
		log.Printf("%v", err)
	}
}

func random(i int) string {
	bytes := make([]byte, i)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func ResetPassword(req *account.UpdatePasswordRequest) error {
	usr, err := db.Read(req.UserId)
	if err != nil {
		return err
	}
	pass, err := base64.StdEncoding.DecodeString(usr.Password)
	if err != nil {
		return err
	}
	// Compare old password
	if err := bcrypt.CompareHashAndPassword(pass, []byte(req.OldPassword)); err != nil {
		return ErrComparePasswords
	}

	h, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), 10)
	if err != nil {
		return err
	}
	pp := base64.StdEncoding.EncodeToString(h)

	// Store new password to user
	err = db.UpdatePassword(req.UserId, pp)
	if err != nil {
		log.Printf("Not found")
		return err
	}

	// Delete old session
	return db.DeleteSession(req.SessionId)
}
