package db

import (
	"fmt"
	mon "gitlab.contetto.io/contetto-micro/db/mongo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"time"

	user "gitlab.contetto.io/contetto-micro/users-srv/src/service/proto/account"
)

var (
	database     string
	collection   string
	session      *mgo.Session
	MongoUsers        *mon.Mongo
	mongoSess    *mon.Mongo
	mongoLog     *mon.Mongo
)

const (
	sessions = "sessions"
	users = "users"
)

func Init() {
	var err error

	log.Println("Mongo Address: " + os.Getenv("MICRO_DATABASE"))
	MongoUsers, err = mon.Init("mongo:27017", "users", users, func(coll *mgo.Collection) {
		// Create unique index for email field
		index := mgo.Index{
			Key:      []string{"email"},
			Unique:   true,
			DropDups: true,
		}

		err = coll.EnsureIndex(index)
		if err != nil {

			return
		}

	})

	if err != nil {
		log.Fatal(err)
	}

	// Try to ping
	log.Printf("Trying to ping")
	fmt.Println(MongoUsers.Ping())
	log.Printf("Was pinged")

	mongoSess, err = mon.Init("mongo:27017", "users", sessions, nil)
	if err != nil {
		log.Fatal(err)
	}

	mongoLog, err = mon.Init("mongo:27017", "users", "logs", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func CreateSession(sess *user.Session) error {
	if sess.Created == 0 {
		sess.Created = time.Now().Unix()
	}

	if sess.Expires == 0 {
		sess.Expires = time.Now().Add(time.Hour * 24 * 7).Unix()
	}

	return mongoSess.Insert(sess)
}

func DeleteSession(id string) error {
	return mongoSess.Remove(bson.M{"sessionid": id})
}

func ReadSession(id string) (user.Session, error) {
	var session user.Session
	err := mongoSess.FindOne(bson.M{"sessionid": id}, &session)
	return session, err
}

func Create(user *user.User, salt string, password string) error {
	user.Created = time.Now().Unix()
	user.Updated = time.Now().Unix()
	return MongoUsers.Insert(user)
}

func Delete(id string) error {
	if !bson.IsObjectIdHex(id) {
		return fmt.Errorf("%s is not a ObjectId value", id)
	}

	return MongoUsers.Remove(bson.M{"id": id})
}

func FindOne(rule bson.M) (user.User, error) {
	var usr user.User
	err := MongoUsers.FindOne(rule, &usr)
	return usr, err
}

func Search(query bson.M) ([]*user.User, error) {
	var usrs []*user.User
	err := MongoUsers.FindAll(query, &usrs)
	return usrs, err
}

func UpdateName(email, oldname, name string) error {
	var acc user.User
	log.Print("email: ", email)
	log.Print("oldname: ", oldname)
	log.Print("newname: ", name)
	err := MongoUsers.FindOne(bson.M{"email": email, "username": oldname}, &acc)
	if err != nil {
		return err
	}
	return nil
	//return coll.Update(bson.M{"email": email, "username": oldname}, bson.M{"$set": bson.M{"username": name}})
}

// GetUserBySession Returns user by the session
func GetUserBySession(sessionID string) (*user.User, error) {
	sess, err := ReadSession(sessionID)
	if err != nil {
		return nil, err
	}

	userId := sess.UserId
	return Read(userId)
}

func GetPassword(email, username string) (string, string, error) {
	var acc user.User
	err := MongoUsers.FindOne(bson.M{"email": email}, &acc)
	if err != nil {
		return "", "", err
	}

	return acc.Password, acc.Username, nil
}

func Read(id string) (*user.User, error) {
	var usr user.User
	if !bson.IsObjectIdHex(id) {
		return nil, fmt.Errorf("%s is not object id", id)
	}

	err := MongoUsers.FindOne(bson.M{"id": id}, &usr)
	return &usr, err
}

func GetAccounts(limit, skip int) ([]user.User, error) {
	var users []user.User
	coll, err := MongoUsers.GetCollection()
	if err != nil {
		return users, err
	}
	err = coll.Find(bson.M{}).Skip(skip).Limit(limit).All(&users)
	return users, err
}

func UpdatePassword(userId string, newPassword string) error {
	return Update(userId, bson.M{"$set": bson.M{"password": newPassword}})
}

func Update(userId string, rule bson.M) error {
	return MongoUsers.Update(bson.M{"id": userId}, rule)
}

func SaltAndPassword(username, email string) (string, string, error) {

	var salt, pass string

	return salt, pass, nil
}

func StoreLog(req *user.StoreEndpointRequest) error {
	return mongoLog.Insert(req)
}

func GetLogs(query bson.M)(endpoints []*user.StoreEndpointRequest, err error) {
	coll, err := mongoLog.GetCollection()
	if err != nil {
		return
	}
	err = coll.Find(bson.M{}).All(&endpoints)
	return
}

func Ping() error {
	return MongoUsers.Ping()
}
