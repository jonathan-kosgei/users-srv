syntax = "proto3";

service Account {
	rpc Create(User) returns (CreateResponse) {}
	rpc Read(ReadRequest) returns (ReadResponse) {}
	rpc Update(UpdateRequest) returns (UpdateResponse) {}
	rpc Delete(DeleteRequest) returns (DeleteResponse) {}
	rpc Search(SearchRequest) returns (SearchResponse) {}
	rpc GetUserByEmail(SearchRequest) returns (SearchResponseOne) {}
	rpc LoginNative(LoginRequest) returns (LoginResponse) {}
	rpc UpdateSession(UpdateSessionRequest) returns (UpdateSessionResponse) {}
	rpc SocialLogin(SocialLoginRequest) returns (SocialLoginResponse) {}
	rpc ResetPassword(ResetPasswordRequest) returns (ResetPasswordResponse) {}
	rpc Logout(LogoutRequest) returns (LogoutResponse) {}
	rpc ValidateUser(ReadSessionRequest) returns(ReadSessionResponse) {}
	rpc GetUserBySession(ReadSessionRequest) returns(UserResponse) {}
	rpc VerifyUser(VerifyRequest) returns (VerifyResponse) {}

	rpc StoreEndpoint(StoreEndpointRequest) returns (StoreEndpointResponse) {}
	rpc GetEndpoints(GetEndpointsRequest) returns (GetEndpointsResponse) {}

	rpc Health(HealthItem) returns (HealthItem) {}
}

message User {
	string id = 1;		// uuid
	string username = 2;	// alphanumeric user or org
	string email = 3;
	int64 created = 4;	// unix
	int64 updated = 5;	// unix
	string firstName = 100;
	string lastName = 101;
	string appId = 6;
	string appSecret = 7;
	string platform = 8;
	string password = 9;
	string mobileNumber = 10;
	string phoneNumber = 11;
	bool receiveNotification = 12;
	string address = 13;
	string city = 14;
	string state = 15;
	string country = 16;
	string zip = 17;
	string salt = 18;
	string profilePhoto = 19;
	string promoCodeID = 20;
	bool joinMailList = 21;
	string organization = 22;
	repeated string whitelistHosts = 23;
	repeated string whitelistIPs = 24;
	string subscriptionStart = 25;
	double subscriptionEnd = 26;
	string subscriptionPlan = 27;
	bool isSubscriptionValid = 28;
	string role = 29;
	bool isEnabled = 30;
	bool isVerified = 31;
	repeated string companies = 32;
	bool sendPassword = 33;
	string companyId = 34;
	string brandId = 35;
	string about = 36;
	string postalCode = 37;
	string oldPassword = 38;
	string newPassword = 39;
	bool notSendEmail = 40;
}

message CreateRequestREST {
	DataUser data = 1;
}

message DataUser {
	User attributes = 1;
}

message Session {
        string id = 1;
        string sessionId = 2;
        string userId = 3;
        string username = 4;
        int64 created = 5;      // unix
        int64 expires = 6;      // unix
        string companyId = 7;
        string brandId = 8;
}

message CreateRequest {
	string id = 1;		// uuid
	string username = 2;	// alphanumeric user or org
	string email = 3;
	int64 created = 4;	// unix
	int64 updated = 5;	// unix
	string appId = 6;
	string appSecret = 7;
	string firstName = 100;
	string lastName = 101;
	string platform = 8;
	string password = 9;
	string mobileNumber = 10;
	string phoneNumber = 11;
	bool receiveNotification = 12;
	string address = 13;
	string city = 14;
	string state = 15;
	string country = 16;
	string zip = 17;
	string salt = 18;
	string profilePhoto = 19;
	string promoCodeID = 20;
	bool joinMailList = 21;
	string organization = 22;
	repeated string whitelistHosts = 23;
	repeated string whitelistIPs = 24;
	string subscriptionStart = 25;
	double subscriptionEnd = 26;
	string subscriptionPlan = 27;
	bool isSubscriptionValid = 28;
	string role = 29;
	bool isEnabled = 30;
	bool isVerified = 31;
	string companyId = 34;
	string brandId = 35;
	string about = 36;
	string postalCode = 37;
	string oldPassword = 38;
	string newPassword = 39;
	bool notSendEmail = 40;
}

message UpdateSessionRequest {
	string companyId = 1;
	string brandId = 2;
	string userId = 3;
	string sessionId = 4;
}

message UpdateSessionRequestREST {
	UpdateSessionData data = 1;
}

message UpdateSessionData {
	UpdateSessionRequest attributes = 1;
	UpdateSessionRelationships relationships = 2;
}

message UpdateSessionRelationships {
	SessionDataName companies = 1;
	SessionDataName brands = 2;
}

message SessionDataName {
	SessionDataItem data = 1;
}

message SessionDataItem {
	string type = 1;
	string id = 2;
}

message UpdateSessionResponse {
	Session session = 1;
}

message UpdateBody {

}

message CreateResponse {
	string id = 1;
	string validationToken = 2;
}

message DeleteRequest {
	string id = 1;
}

message DeleteResponse {
}

message ReadRequest {
	string id = 1;
}

message ReadResponse {
	User user = 1;
}

message UpdateRequest {
	string id = 1;
	User user = 2;
	string sessionId = 3;
}

message UpdateResponse {
	string id = 1;
}

message UpdatePasswordRequest {
        string userId = 1;
        string oldPassword = 2;
        string newPassword = 3;
        string confirmPassword = 4;
				string sessionId = 5;
}

message UpdatePasswordResponse {
}

message SearchRequest {
	string username = 1;
	string email = 2;
	int64 limit = 3;
	int64 offset = 4;
	string country = 5;
	string city = 7;
}

message SearchResponse {
	repeated User users = 1;
}

message SearchResponseOne {
	User user = 1;
}

message ReadSessionRequest {
        string sessionId = 1;
}

message ReadSessionRequestREST {
	DataSessions data = 1;
}

message DataSessions {
	ReadSessionRequest attributes = 1;
}

message ReadSessionResponse {
        Session session = 1;
}

message LoginRequest {
        string email = 1;
        string password = 2;
}

message SocialLoginRequest {
	string email = 1;
}

message SocialLoginResponse {
	Session session = 1;
}

message LoginRequestREST {
	Data data = 1;
}

message Data {
	LoginRequest attributes = 1;
}

message LoginResponse {
        Session session = 1;
}

message LogoutRequest {
        string sessionId = 1;
}

message LogoutResponse {
}

message ResetPasswordRequest {
	string oldPassword = 1;
	string newPassword = 2;
	string userID = 3;
	string sessionID = 4;
}

message ResetPasswordRequestREST {
	DataResetPass  data = 1;
}

message DataResetPass {
	AttributesResetPass attributes = 1;
}

message AttributesResetPass {
	string oldPassword = 1;
	string newPassword = 2;
}

message ResetPasswordResponse {
	Session session = 1;
}

message VerifyRequest {
	string token = 1;
}

message VerifyResponse {

}

message UserResponse {
	User user = 1;
}

message StoreEndpointRequest {
	string endpoint = 1;
	string date = 2;
	string userId = 3;
	string id = 4;
}

message StoreEndpointRequestREST {
	StoreEndpointData data = 1;
}

message StoreEndpointData {
	StoreEndpointRequest attributes = 1;
}

message StoreEndpointResponse {
	string id = 1;
}

message GetEndpointsRequest {
	string userId = 1;
}

message GetEndpointsResponse {
	repeated StoreEndpointRequest logs = 1;
}

message HealthItem {
	string result = 1;
}
