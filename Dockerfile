FROM golang:1.6.0

ADD . /go/src/gitlab.contetto.io/contetto-micro/users-srv

WORKDIR /go/src/gitlab.contetto.io/contetto-micro/users-srv/src/service

RUN pwd; ls -la

RUN go get --insecure ./...

EXPOSE     8080 8081 

ENTRYPOINT ["/go/src/gitlab.contetto.io/contetto-micro/users-srv/src/bin/service", "--registry=kubernetes"]