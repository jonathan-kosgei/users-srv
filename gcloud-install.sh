curl https://get.docker.com/ | sh
service docker start
cd /opt
wget -P /opt --quiet https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-120.0.0-linux-x86_64.tar.gz
tar -xvf /opt/google-cloud-sdk-120.0.0-linux-x86_64.tar.gz
./google-cloud-sdk/install.sh